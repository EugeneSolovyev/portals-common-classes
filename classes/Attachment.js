import File from './File';
import {bindThis} from 'Common';

export default class Attachment {
	/**
	 * @class
	 * @param id {number}
	 * @param type {number}
	 * @param file {File}
	 * @param state {Object}
	 */
	constructor(id, type, file, state) {
		this.id = id;
		this.type = type;
		this.file = file;
		this.state = state;

		this.version = 0;

		bindThis([
			this.getId,
			this.getFile,
			this.getType,
			this.setFile,
			this.isScorefull,
			this.canAddScore,
			this.setScorefullness,
		], this);
	}

	static get scorefullTypes() {
		return [
			ENUMS.ATTACHMENTS_TYPES.BUTTON,
			ENUMS.ATTACHMENTS_TYPES.TIMER,
			ENUMS.ATTACHMENTS_TYPES.CHECKLIST,
			ENUMS.ATTACHMENTS_TYPES.RIDDLE,
			ENUMS.ATTACHMENTS_TYPES.STOPWATCH,
			ENUMS.ATTACHMENTS_TYPES.TEST,
		];
	}

	static parse(rawObject) {
		let id = +rawObject.id || 0;
		let type = +rawObject.type;
		let file = File.parse(rawObject.file || {});
		let state = _.get(rawObject, 'mapItemAttachments.state',
			_.get(rawObject, 'mapViewer_mapItemAttachments.state', {}));
		return new Attachment(id, type, file, state);
	}

	setScorefullness(isTrue) {
		this.state.canAddScore = isTrue;
	}

	canAddScore() {
		return this.state.canAddScore;
	}

	isScorefull() {
		return ~Attachment.scorefullTypes.indexOf(this.getType());
	}

	getType() {
		return this.type;
	}

	getFile() {
		return this.file;
	}

	getId() {
		return this.id;
	}

	getRawObject() {
		return {
			type: this.type,
			fileId: this.file.getId(),
			version: this.version,
		};
	}

	setFile(file) {
		this.file = file;
	}
}