import Item from './Item';
import { bindThis } from 'Common';

export default class Category {
	/**
	 * @class
	 * @param id {number}
	 * @param name {string}
	 * @param items {Item[]}
	 * @param type {number}
	 */
	constructor(id, name, items, type) {
		/** @private */ this.id = id;
		/** @private */ this.name = name;
		/** @private */ this.items = items;
		/** @private */ this.type = type;

		this.version = 0;

		let bindList = [
			this.itemsMap,
			this.getId,
			this.getName,
			this.setName,
			this.getItemById,
			this.isBackground,
			this.removeItem,
			this.getType,
		];

		bindThis(bindList, this);
	}


	/**
	 * @param rawObject {object}
	 */
	static parse(rawObject) {
		let id = +rawObject.id;
		let name = '' + rawObject.name;
		let items = (rawObject.items || []).map(item => Item.parse(item));
		let type = +(rawObject.type || ENUMS.ITEM_TYPES.USUAL);
		return new Category(id, name, items, type);
	}

	static mergeChanges(target, changes) {
		if (!_.isUndefined(changes.name)) target.setName(changes.name);
		target.version++;
	}

	getType() {
		return this.type;
	}

	removeItem(item) {
		this.items = this.items.filter(itemInCategory => itemInCategory.getId() !== item.getId());

	}

	isBackground() {
		return this.type === ENUMS.ITEM_TYPES.BACKGROUND;
	}

	/**
	 * @param iterator {function}
	 */
	itemsMap(iterator) {
		return this.items.map(iterator);
	}

	getId() {
		return this.id;
	}

	getName() {
		return this.name;
	}

	setName(name) {
		this.name = '' + name;
	}

	/**
	 * @param newItem {Item}
	 */
	addItem(newItem) {
		this.items.unshift(newItem);
		this.version++;
	}

	/**
	 * @param itemId {number}
	 * @return {Item|null}
	 */
	getItemById(itemId) {
		return _.find(this.items, item => item.getId() === itemId) || null;
	}
}