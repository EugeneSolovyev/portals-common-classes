/**
 * Bind to this array of functions.
 *
 * @param functions {array}
 * @param instance
 */
export function bindThis(functions, instance) {
	functions.map(functionInst => {
		instance[functionInst.name] = functionInst.bind(instance);
	});
}