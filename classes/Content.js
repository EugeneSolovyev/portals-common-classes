import Attachment from './Attachment';
import { bindThis, findAndReplace } from 'Common';

export default class Content {
	/**
	 * @class
	 * @param id {number}
	 * @param itemId {number}
	 * @param anons {string}
	 * @param attachments {Attachment[]}
 	 * @param status {number}
	 */
	constructor(id, itemId, anons, attachments, status) {
		this.id = id;
		this.itemId = itemId;
		this.anons = anons;
		this.attachments = attachments;
		this.status = status;
		this.version = 0;

		bindThis([
			this.getId,
			this.getAnons,
			this.getAttachments,
			this.getRawObject,
			this.clone,
			this.setAnons,
			this.updateAttachment,
			this.versionUp,
			this.getStatus
		], this);
	}

	static parse(rawObject) {
		let id = +(rawObject.id || 0);
		let itemId = +rawObject.itemId;
		let anons = '' + (rawObject.anons || '');
		let attachments = (rawObject.attachments || []).map(attachment => Attachment.parse(attachment));
		let status = +(rawObject.status || ENUMS.CONTENT_STATUS.NOT_VIEWED.ID);
		return new Content(id, itemId, anons, attachments, status);
	}

	getRawObject() {
		let result = {
			id: this.getId(),
			itemId: this.getItemId(),
			anons: this.getAnons(),
			status: this.getStatus(),
			attachments: this.getAttachments().map(attach => attach.getId())
		};
		return result;
	}

	setAnons(newAnons) {
		this.anons = '' + newAnons;
		this.version++;
	}

	setAttachments(newAttach) {
		this.attachments = newAttach;
	}

	setStatus(status) {
		this.versionUp();
		this.status = status;
	}

	clone() {
		return Content.parse(this.getRawObject());
	};

	getItemId() {
		return this.itemId;
	}

	getId() {
		return this.id;
	}

	getAnons() {
		return this.anons;
	}

	getAttachments() {
		return this.attachments;
	}
	getStatus() {
		return this.status
	}

	updateAttachment(newAttachment) {
		this.attachments = findAndReplace(this.attachments, newAttachment, (item, newItem) => item.getId() === newItem.getId());
		this.versionUp();
	}

	versionUp() {
		this.version++;
	}
}