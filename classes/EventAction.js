const bindList = [
	'getRawObject',
	'modify',
	'modifyValue',
	'modifyOperation',
	'getValue',
	'getRawObject',
	'clone',
	'removeSubAction',
	'addSubAction',
	'runYourSelf',
	'getOperationId',
];

export default class EventAction {
	/**
	 * @class
	 *
	 * @param value {any}
	 * @param operation {object}
	 */
	constructor(operation, value) {
		this.operation = operation;
		this.value = value;
		this.bindMe.bind(this)();
	}

	bindMe() {
		_.forEach(bindList, (value) => {
			this[value] = this[value].bind(this);
		});
	}

	static parse(rawObject) {
		let operation = rawObject.operation;
		let value;
		if (_.isArray(rawObject.value)) {
			value = rawObject.value.map(filter => EventAction.parse(filter));
		} else {
			value = rawObject.value;
		}
		return new EventAction(operation, value);
	}

	static isSimple(operation) {
		return operation.TYPE === ENUMS.ACTION_OPERATION_TYPES.SIMPLE;
	}

	static isCombined(operation) {
		return operation.TYPE === ENUMS.ACTION_OPERATION_TYPES.COMBINED;
	}

	static needChangedValueType(oldVal, newVal) {
		return oldVal.TYPE !== newVal.TYPE;
	}

	static getDefaultValue(operation) {
		if (EventAction.isCombined(operation)) return [];
		if (EventAction.isSimple(operation)) return 0;
	}

	static getDefaultAction() {
		return EventAction.parse({
			operation: ENUMS.ACTION_OPERATIONS.STATUS_CHANGE,
			value: 0
		});
	}

	removeSubAction(index) {
		if (EventAction.isCombined(this.operation)) {
			this.value.splice(index, 1);
		}
	}

	addSubAction(newSubaction) {
		if (EventAction.isCombined(this.operation)) {
			this.value.push(newSubaction);
		}
	}

	getValue() {
		return this.value;
	}

	clone() {
		return new EventAction(this.operation, this.value);
	}

	getRawObject() {
		return {
			operation: this.operation,
			value: this.value
		};
	}

	getOperationId() {
		return this.operation.ID;
	}

	modifyValue(data) {
		if (EventAction.isSimple(this.operation)) {
			this.value = +data.value;
		} else if (EventAction.isCombined(this.operation)) {
			this.value = data.value;
		}
	}

	modifyOperation(data) {
		if (EventAction.needChangedValueType(this.operation, data.value)) {
			this.value = EventAction.getDefaultValue(data.value);
		}
		this.operation = data.value;
	}


	modify(data) {
		if (data.name === 'value') {
			this.modifyValue(data);
		} else if (data.name === 'operation') {
			this.modifyOperation(data);
		}
	}

	runYourSelf(item, projectEventHandler, transitionChain, newTransitionContainer) {
		switch (this.operation.ID) {
			case ENUMS.ACTION_OPERATIONS.STATUS_CHANGE.ID:
				return new Promise((resolve, reject) => {
					let startTransition = item.setStatus(this.value);
					startTransition.then(resolve);
					transitionChain.then(startTransition);
					newTransitionContainer.newTransition = startTransition;
				});
			case ENUMS.ACTION_OPERATIONS.COMBINED_SYNC.ID:
				return new Promise((resolve, reject) => {
					syncCallPromises().then(resolve).catch(reject);

					async function syncCallPromises() {
						let callbacks = [];

						for (let key in this.value) {
							let cb = await this.value[+key].runYourSelf(item);
							callbacks.push(cb);
						}

						return callbacks;
					}
					//let i = -1;
					// function callNext(prevCb) {
					// 	if (prevCb) callbacks.push(prevCb);
					// 	i++;
					// 	if (i > this.value.length - 1) {
					// 		resolve(callbacks);
					// 	} else {
					// 		this.value[i].runYourSelf.then(callNext).catch(err => reject(err));
					// 	}
					// }
				});

			case ENUMS.ACTION_OPERATIONS.COMBINED_ASYNC.ID:
				return Promise.all(this.value.map(action => action.runYourSelf(item)));
		}
	}
}