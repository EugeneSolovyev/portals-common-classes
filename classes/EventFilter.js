export default class EventFilter {
	/**
	 * @class
	 * @param operation {Object}
	 * @param value {any[]}
	 */
	constructor(operation, value) {
		this.operation = operation;
		this.value = value;

		this.getRawObject = this.getRawObject.bind(this);
		this.modify = this.modify.bind(this);
		this.modifyValue = this.modifyValue.bind(this);
		this.modifyOperation = this.modifyOperation.bind(this);
		this.clone = this.clone.bind(this);
		this.getValue = this.getValue.bind(this);

		this.removeSubfilter = this.removeSubfilter.bind(this);
		this.addSubfilter = this.addSubfilter.bind(this);
		this.checkYourSelf = this.checkYourSelf.bind(this);
		this.checkForScoreChanging = this.checkForScoreChanging.bind(this);
		this.checkForItemStatusChanging = this.checkForItemStatusChanging.bind(this);
		this.checkForContentStatusChanging = this.checkForContentStatusChanging.bind(this)
	}

	static parse(rawObject) {
		let operation = rawObject.operation;
		let value;
		if (EventFilter.isCombinedOperation(operation)) {
			value = rawObject.value.map(filter => EventFilter.parse(filter));
		} else {
			value = rawObject.value;
		}
		return new EventFilter(operation, value);
	}

	static isDigitalOperation(operation) {
		return operation.TYPE === ENUMS.FILTER_OPERATION_TYPES.NUMERIC;
	}

	static isCombinedOperation(operation) {
		return operation.TYPE === ENUMS.FILTER_OPERATION_TYPES.COMBINED;
	}

	static needChangedValueType(oldVal, newVal) {
		return oldVal.TYPE !== newVal.TYPE;
	}

	static getDefaultValue(operation) {
		if (EventFilter.isCombinedOperation(operation)) return [];
		if (EventFilter.isDigitalOperation(operation)) return [0,0];
	}

	static getDefaultFilter() {
		return EventFilter.parse({
			operation: ENUMS.FILTER_OPERATIONS.LESS,
			value: [0, 0]
		});
	}

	removeSubfilter(index) {
		if (EventFilter.isCombinedOperation(this.operation)) {
			this.value.splice(index, 1);
		}
	}

	addSubfilter(newSubfilter) {
		if (EventFilter.isCombinedOperation(this.operation)) {
			this.value.push(newSubfilter);
		}
	}

	getValue() {
		return this.value;
	}

	clone() {
		return new EventFilter(this.operation, this.value);
	}

	getRawObject() {
		return {
			operation: this.operation,
			value: this.value
		};
	}

	modifyValue(data) {
		if (EventFilter.isDigitalOperation(this.operation)) {
			this.value = data.value.map(number => +number);
		} else if (EventFilter.isCombinedOperation(this.operation)) {
			this.value = data.value;
		}
	}

	modifyOperation(data) {
		if (EventFilter.needChangedValueType(this.operation, data.value)) {
			this.value = EventFilter.getDefaultValue(data.value);
		}
		this.operation = data.value;
	}


	modify(data) {
		if (data.name === 'value') {
			this.modifyValue(data);
		} else if (data.name === 'operation') {
			this.modifyOperation(data);
		}
	}

	checkForScoreChanging(value) {
		switch (this.operation.ID) {
			case ENUMS.FILTER_OPERATIONS.EQUAL.ID:
				return this.value[0] === +value;

			case ENUMS.FILTER_OPERATIONS.NOT_EQUAL.ID:
				return this.value[0] !== +value;

			case ENUMS.FILTER_OPERATIONS.LESS.ID:
				return +value < this.value[0];

			case ENUMS.FILTER_OPERATIONS.LESS_OR_EQUAL.ID:
				return +value <= this.value[0];

			case ENUMS.FILTER_OPERATIONS.MORE.ID:
				return +value > this.value[0];

			case ENUMS.FILTER_OPERATIONS.MORE_OR_EQUAL.ID:
				return +value >= this.value[0];

			case ENUMS.FILTER_OPERATIONS.OR.ID:
				return this.value.reduce((sum, element) => {
						return sum || element.checkForScoreChanging(value);
					}, false
				);

			case ENUMS.FILTER_OPERATIONS.AND.ID:
				return this.value.reduce((sum, element) => {
						return sum && element.checkForScoreChanging(value);
					}, true
				);
		}
	}

	checkForItemStatusChanging(itemId, itemStatus) {
		switch (this.operation.ID) {
			case ENUMS.FILTER_OPERATIONS.EQUAL.ID:
				return this.value[1] === +itemStatus && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.NOT_EQUAL.ID:
				return this.value[1] !== +itemStatus && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.LESS.ID:
				return +itemStatus < this.value[1] && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.LESS_OR_EQUAL.ID:
				return +itemStatus <= this.value[1] && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.MORE.ID:
				return +itemStatus > this.value[1] && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.MORE_OR_EQUAL.ID:
				return +itemStatus >= this.value[1] && this.value[0] === itemId;

			case ENUMS.FILTER_OPERATIONS.OR.ID:
				return this.value.reduce((sum, element) => {
						return sum || element.checkForItemStatusChanging(itemId, itemStatus);
					}, false
				);

			case ENUMS.FILTER_OPERATIONS.AND.ID:
				return this.value.reduce((sum, element) => {
						return sum && element.checkForItemStatusChanging(itemId, itemStatus);
					}, true
				);
		}
	}

	checkForContentStatusChanging(contentId, contentStatus) {
		switch (this.operation.ID) {
			case ENUMS.FILTER_OPERATIONS.EQUAL.ID:
				return this.value[1] === +contentStatus && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.NOT_EQUAL.ID:
				return this.value[1] !== +contentStatus && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.LESS.ID:
				return +contentStatus < this.value[1] && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.LESS_OR_EQUAL.ID:
				return +contentStatus <= this.value[1] && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.MORE.ID:
				return +contentStatus > this.value[1] && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.MORE_OR_EQUAL.ID:
				return +contentStatus >= this.value[1] && this.value[0] === contentId;

			case ENUMS.FILTER_OPERATIONS.OR.ID:
				return this.value.reduce((sum, element) => {
						return sum || element.checkForItemStatusChanging(contentId, contentStatus);
					}, false
				);

			case ENUMS.FILTER_OPERATIONS.AND.ID:
				return this.value.reduce((sum, element) => {
						return sum && element.checkForItemStatusChanging(contentId, contentStatus);
					}, true
				);
		}
	}

	checkForClickOnItem(value) {
		switch (this.operation.ID) {
			case ENUMS.FILTER_OPERATIONS.EQUAL.ID:
				return this.value[0] === +value;

			case ENUMS.FILTER_OPERATIONS.NOT_EQUAL.ID:
				return this.value[0] !== +value;

			case ENUMS.FILTER_OPERATIONS.LESS.ID:
				return +value < this.value[0];

			case ENUMS.FILTER_OPERATIONS.LESS_OR_EQUAL.ID:
				return +value <= this.value[0];

			case ENUMS.FILTER_OPERATIONS.MORE.ID:
				return +value > this.value[0];

			case ENUMS.FILTER_OPERATIONS.MORE_OR_EQUAL.ID:
				return +value >= this.value[0];

			case ENUMS.FILTER_OPERATIONS.OR.ID:
				return this.value.reduce((sum, element) => {
						return sum || element.checkForClickOnItem(value);
					}, false
				);

			case ENUMS.FILTER_OPERATIONS.AND.ID:
				return this.value.reduce((sum, element) => {
						return sum && element.checkForClickOnItem(value);
					}, true
				);
		}
	}

	/**
	 *
	 * @param value - {any[]}
	 * @param eventType
	 * @return {*}
	 */
	checkYourSelf(values, eventType) {
		switch (eventType) {
			case ENUMS.EVENT_TYPES.SCORE_CHANGED.ID:
				return this.checkForScoreChanging(values[0]);
			case ENUMS.EVENT_TYPES.ITEM_STATUS_CHANGED.ID:
				return this.checkForItemStatusChanging(values[0], values[1]);
			case ENUMS.EVENT_TYPES.CLICK_ON_ITEM.ID:
				return this.checkForClickOnItem(values[0]);
			case ENUMS.EVENT_TYPES.CONTENT_STATUS_CHANGED.ID:
				return this.checkForContentStatusChanging(values[0], values[1]);
		}
	}


}