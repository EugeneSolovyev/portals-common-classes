import defaultImage from 'Content/svg/noImage.svg';
import {bindThis} from 'Common';

export default class File {
	/**
	 * @class
	 * @param id {number}
	 * @param type {number}
	 * @param categoryId {number}
	 * @param value {string}
	 * @param settings {object}
	 */
	constructor(id, type, categoryId, value, settings) {
		this.id = id;
		this.type = type;
		this.categoryId = categoryId;
		this.value = value;
		this.settings = settings;

		bindThis([
			this.getId,
			this.getValue,
			this.getType,
			this.getCategory,
			this.getImageValue,
			this.getJsonValue,
			this.getSettings,
			this.isLottie,
		], this);
	}

	isLottie() {
		return this.getType() === ENUMS.FILE_TYPES.LOTTIE;
	}

	getSettings() {
		return this.settings;
	}

	getCategory() {
		return this.categoryId;
	}

	getId() {
		return this.id;
	}

	getType() {
		return this.type;
	}

	getValue() {
		switch (this.type) {
			case ENUMS.FILE_TYPES.IMAGE:
				return this.getImageValue();

			case ENUMS.FILE_TYPES.JSON:
				return this.getJsonValue();

			case ENUMS.FILE_TYPES.LOTTIE:
			default:
				return URLES.ServerURL + this.value;
		}
	}

	getImageValue() {
		return _.isEmpty(this.value) ? defaultImage : URLES.ServerURL + this.value;
	}

	getJsonValue() {
		return JSON.parse(this.value);
	}

	static parse(rawObject) {
		let id = +rawObject.id;
		let type = +rawObject.type;
		if (_.isNaN(type)) type = ENUMS.FILE_TYPES.IMAGE;
		let categoryId = +rawObject.categoryId;
		let value = '' + (rawObject.value || '');
		let settings = rawObject.settings || {};
		return new File(id, type, categoryId, value, settings);
	}


}