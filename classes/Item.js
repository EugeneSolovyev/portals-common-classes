import Store from './Store';
import Listener from './Listener';
import File from './File';
import StatusMap from './StatusMap';
import Attachment from './Attachment';
import {bindThis} from './Common';
import EventFilter from './EventFilter';
import EventAction from './EventAction';
import Backinator from 'Backinator';
import TransitionAnimation from './TransitionAnimation';

const defaultStatusMap = {
	size: 0.05,
	position: {
		x: 0.5,
		y: 0.5
	},
	currentFrame: 0
};
const defaultFrame = File.parse({});

export default class Item extends Store {
	/**
	 * @class Item
	 * @param id {number}
	 * @param name {string}
	 * @param categoryId {number}
	 * @param listeners {Listener[]}
	 * @param frames {File[]}
	 * @param mapId {number}
	 * @param statusMap {Object}
	 * @param status {number}
	 * @param attachments {Attachment[]}
	 * @param type {number}
	 * @param additionalInfo {object}
	 * @param size {number}
	 * @param transitions {TransitionAnimation[]}
	 */
	constructor(id, name, categoryId, listeners, frames, mapId, status, statusMap, attachments, type, additionalInfo,
		size, transitions) {
		super();

		this.id = id;
		this.name = name;
		this.categoryId = categoryId;
		this.listeners = listeners;
		this.frames = frames;
		this.mapId = mapId;
		this.status = status;
		this.statusMap = statusMap;
		this.attachments = attachments;
		this.type = type;
		this.additionalInfo = additionalInfo;
		this.size = size;
		this.transitions = transitions;

		this.version = 0;
		this.listenersVersion = 0;

		let bindList = [
			this.setId,
			this.setName,
			this.setMapId,
			this.setPosition,
			this.setLottieSettings,
			this.setSize,
			this.setRotation,
			this.setStatus,
			this.setFrames,
			this.getFrames,
			this.setCurrentFrame,
			this.setAdditionalInfo,
			this.setStep,
			this.setFontSize,
			this.setFontPosition,
			this.setFontPositionX,
			this.setFontPositionY,
			this.setFontColor,
			this.setFontWeight,
			this.initializeStepListeners,
			this.initializeStepStatuses,
			this.changeAnimationAction,
			this.changeTransitionFrom,
			this.changeTransitionTo,
			this.addTransitionAction,
			this.addTransition,
			this.removeTransition,
			this.removeTransitionAction,
			this.findTransition,
			this.bindClassWithComponent,

			this.getId,
			this.getFrameByIndex,
			this.getFrameById,
			this.getName,
			this.getCategory,
			this.getPosition,
			this.getLottieSettings,
			this.getCurrentFrame,
			this.getFrameByStatus,
			this.getSize,
			this.getRotation,
			this.getMapId,
			this.getCurrentConfig,
			this.getConfigByIndex,
			this.getStatus,
			this.getStatusMap,
			this.getListenerById,
			this.getListeners,
			this.getNewFrame,
			this.getNextFrame,
			this.getPreviousFrame,
			this.getType,
			this.getStep,
			this.getAdditionalInfo,
			this.isBackground,
			this.isStep,
			this.getFontSize,
			this.getFontPosition,
			this.getFontColor,
			this.getFontWeight,
			this.getTransitions,

			this.setAttachmentList,
			this.getAttachmentList,
			this.getAttachmentById,
			this.showAttachments,
			this.updateAttachments,
			this.removeAttachment,

			this.clone,
			this.addStatus,
			this.addListener,
			this.updateListener,
			this.updateListenersVersion,
			this.framesMap,
			this.calculateSize,
			this.makeRawObject,
			this.isStatusAvailable,
			this.handleTheEvent,
			this.removeListener,
			this.isItemOnMap,
			this.deleteFrame,

			this.versionUp
		];

		bindThis(bindList, this);
	}

	/**
	 *
	 * @param rawObject {object}
	 * @returns {Item}
	 */
	static parse(rawObject) {
		let id = +rawObject.id;
		let name = '' + rawObject.name;
		let categoryId = +rawObject.categoryId;

		let mapId = +(rawObject.mapId || -1);
		let status = +rawObject.status;
		if (_.isNaN(status)) status = 0;

		let listeners = (rawObject.listeners || []).map(listener => Listener.parse(listener));
		let frames = (rawObject.frames || []).map(frame => File.parse(frame));
		if (!rawObject.statusMap || !rawObject.statusMap.length) {
			rawObject.statusMap = [defaultStatusMap];
			_.head(rawObject.statusMap).currentFrame = _.head(frames).getId();
		}
		let statusMap = rawObject.statusMap.map(status => StatusMap.parse(status));
		let attachments = (rawObject.attachments || []).map(attachment => Attachment.parse(attachment));
		let type = +rawObject.type || ENUMS.ITEM_TYPES.USUAL;
		let additionalInfo = rawObject.additionalInfo || {};
		let size;
		if (!_.isUndefined(rawObject.size)) size = +rawObject.size;
		let transitions = (rawObject.transitions || []).map(transition => TransitionAnimation.parse(transition));

		return new Item(id, name, categoryId, listeners, frames, mapId, status, statusMap, attachments, type,
			additionalInfo, size, transitions);
	}

	static mergeChanges(target, changes) {
		if (!_.isUndefined(changes.name)) target.setName(changes.name);
		if (!_.isUndefined(changes.position)) target.setPosition(changes.position);
		if (!_.isUndefined(changes.size)) target.setSize(changes.size);
		if (!_.isUndefined(changes.rotation)) target.setRotation(changes.rotation);
		if (!_.isUndefined(changes.status)) target.setStatus(changes.status);
		if (!_.isUndefined(changes.frames)) target.setFrames(changes.frames);
		if (!_.isUndefined(changes.currentFrame)) target.setCurrentFrame(changes.currentFrame);
		if (!_.isUndefined(changes.additionalInfo)) target.setAdditionalInfo(changes.additionalInfo);
		if (!_.isUndefined(changes.lottieSettings)) target.setLottieSettings(changes.lottieSettings);
		target.version++;
	}

	bindClassWithComponent(component) {
		this.component = component;
		this.getTransitions().forEach(transition => transition.bindClassWithComponent(component));
	}

	getType() {
		return this.type;
	}

	isBackground() {
		return this.getType() === ENUMS.ITEM_TYPES.BACKGROUND;
	}

	isStep() {
		return this.getType() === ENUMS.ITEM_TYPES.STEP;
	}

	getStep() {
		return this.getAdditionalInfo().stepNumber;
	}

	setStep(newStep) {
		this.getListeners()
			.filter(listener => listener.isStepListener())
			.forEach(listener => {listener.getEventFilter().modifyValue({value: [+newStep - 1]});});
		this.getAdditionalInfo().stepNumber = +newStep;
		this.versionUp();
	}

	getFontSize() {
		return this.getCurrentConfig().getFontSize();
	}

	setFontSize(newSize) {
		this.getCurrentConfig().setFontSize(+newSize);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	setFontPosition(newPositions) {
		this.getCurrentConfig().setFontPosition(newPositions);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	setFontPositionX(newPosition) {
		this.getCurrentConfig().setFontPositionX(+newPosition);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	setFontPositionY(newPosition) {
		this.getCurrentConfig().setFontPositionY(+newPosition);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	getFontPosition() {
		return this.getCurrentConfig().getFontPosition();
	}

	getFontColor() {
		return this.getCurrentConfig().getFontColor();
	}

	setFontColor(color) {
		this.getCurrentConfig().setFontColor(color);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	getFontWeight() {
		return this.getCurrentConfig().getFontWeight();
	}

	setFontWeight(fontWeight) {
		this.getCurrentConfig().fontWeight = +fontWeight;
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	getTransitions() {
		return this.transitions;
	}

	addTransitionAction(type, transitionIndex, blockIndex) {
		this.transitions[transitionIndex].addAction(type, blockIndex);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	removeTransitionAction(transitionIndex, blockIndex, actionIndex) {
		this.transitions[transitionIndex].removeAction(blockIndex, actionIndex);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	changeAnimationAction(transitionIndex, blockIndex, actionIndex, fieldName, value) {
		this.transitions[transitionIndex].updateAnimationAction(blockIndex, actionIndex, fieldName, value);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	addTransition() {
		let newTransition = TransitionAnimation.parse({});
		if (this.component) newTransition.bindClassWithComponent(this.component);
		this.transitions.unshift(TransitionAnimation.parse({}));
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	removeTransition(transitionIndex) {
		this.transitions.splice(transitionIndex, 1);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	changeTransitionFrom(transitionIndex, value) {
		if (!this.isStatusAvailable(value)) return;
		this.transitions[transitionIndex].setStatusFrom(value, this.getStatusMap()[value]);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	changeTransitionTo(transitionIndex, value) {
		if (!this.isStatusAvailable(value)) return;
		this.transitions[transitionIndex].setStatusTo(value, this.getStatusMap()[value]);
		this.versionUp();
		super.Store.dispatch({type: 'ABRUIGLALBALA'});
	}

	initializeStepStatuses() {
		this.addStatus();
		this.addStatus();
		this.setStatus(1);
		this.setCurrentFrame(this.getFrameByIndex(1).getId());
		this.setStatus(2);
		this.setCurrentFrame(this.getFrameByIndex(2).getId());
		this.setStatus(0);
	}

	initializeStepListeners() {
		const THIS = this;
		makeListener(ENUMS.FILTER_OPERATIONS.LESS, 0);
		makeListener(ENUMS.FILTER_OPERATIONS.EQUAL, 1);
		makeListener(ENUMS.FILTER_OPERATIONS.MORE, 2);

		function makeListener(operation, frame) {
			THIS.addListener(Listener.parse({
				id: Date.now(),
				eventType: ENUMS.EVENT_TYPES.SCORE_CHANGED.ID,
				eventFilter: EventFilter.parse({
					operation: operation,
					value: [0]
				}),
				eventAction: EventAction.parse({
					operation: ENUMS.ACTION_OPERATIONS.STATUS_CHANGE,
					value: frame
				}),
				once: false,
				done: false,
				eventTryNumber: 1,
				additionalInfo: {
					isStepListener: true,
				},
			}));
		}
	}

	setAdditionalInfo(newInfo) {
		this.additionalInfo = newInfo;
	}

	getAdditionalInfo() {
		return this.additionalInfo;
	}

	removeAttachment(badAttachment) {
		this.setAttachmentList(
			this.getAttachmentList()
				.filter(Attachment => Attachment.getId() !== badAttachment.getId()));
		this.versionUp();
	}

	updateAttachments(newAttachments) {
		this.setAttachmentList(newAttachments);
		this.versionUp();
	}

	getAttachmentList() {
		return this.attachments;
	}

	setAttachmentList(AttachmentList) {
		this.attachments = AttachmentList;
	}

	/**
	 * @param eventType {ENUMS.EVENT_TYPES}
	 * @param projectEventHandler {function}
	 * @param values {any[]}
	 * @param updateMap{function}
	 * @param transitionChain -{tipa_Promise}
	 */
	handleTheEvent(eventType, values, projectEventHandler, updateMap, transitionChain = Promise.resolve()) {
		let THIS = this;
		let oldStatus = this.getStatus();
		let newTransitionContainer = {newTransition: false};
		Promise.all(this.listeners.map(listener =>
			listener.handleTheEvent(eventType, values, THIS, projectEventHandler, transitionChain, newTransitionContainer)
		))
			.then(res => {
			})
			.catch(err => {
				window.toastError('Handle event error', err);
			});
		if (oldStatus !== THIS.getStatus()) {
			Backinator.updateItemStatus(THIS.getId(), THIS.getStatus());
			projectEventHandler(ENUMS.EVENT_TYPES.ITEM_STATUS_CHANGED.ID, [THIS.getId(), THIS.getStatus()],
				newTransitionContainer.newTransition);
		}
	}

	setStatus(status) {
		let oldStatus = this.getStatus();
		const THIS = this;
		let transition = this.findTransition(this.getStatus(), status);
		this.status = +status;
		let needToVersionUp = oldStatus !== this.status;
		let thens = [];

		let makeTransitionPromise = () => {
			return new Promise((res) => {
				if (THIS.isStatusAvailable(status)) {
					if (transition) {
						transition.play()
							.then(() => {
								if (needToVersionUp) {
									updateStatusAndRedraw();
								}
								res();
							});
					} else {
						if (needToVersionUp) {
							updateStatusAndRedraw();
						}
						res();
					}
				} else {
					res();
				}
			})
				.then(() => {
					_.forEach(thens, then => then());
				});
		};

		makeTransitionPromise.then = function (callback) {
			thens.push(callback);
		};

		return makeTransitionPromise;

		function updateStatusAndRedraw() {
			THIS.version++;
			THIS.status = +status;
			setTimeout(() => {super.Store.dispatch({type: 'FAKE_ACTION_FOR_UPDATING_MAP'});}, 0);
		}
	}

	showAttachments() {
		let attachments = this.getAttachmentList();
		super.Store.dispatch({
			type: ACTION_TYPES.SHOW_ATTACHMENTS_MODAL,
			payload: attachments
		});
	}

	getAttachmentById(id) {
		return this.getAttachmentList().find(attachment => attachment.getId() === id);
	}


	isItemOnMap() {
		return this.mapId >= 0;
	}

	addStatus() {
		this.version++;
		this.statusMap.push(_.last(this.statusMap).clone());
	}

	removeStatus(index) {
		if (this.statusMap.length === 1) return;
		this.version++;
		this.statusMap.splice(index, 1);
		let currentStatus = this.getStatus();
		currentStatus -= (currentStatus === 0) ? 0 : 1;
		this.setStatus(currentStatus);
	}

	clone() {
		let statusMap = this.statusMap.map(status => status.clone());
		let transitions = TransitionAnimation.parse(this.transitions);
		return new Item(this.id, this.name, this.categoryId, this.listeners, this.frames, this.mapId, this.status,
			statusMap, transitions);
	}

	isStatusAvailable(status) {
		return this.statusMap[+status];
	}

	findTransition(statusFrom, statusTo) {
		return _.find(this.getTransitions(), {
			statusFrom: +statusFrom,
			statusTo: +statusTo
		});
	}

	getStatus() {
		return this.status;
	}

	getStatusMap() {
		return this.statusMap;
	}

	getSize() {
		return _.isUndefined(this.size) ? this.getCurrentConfig().getSize() : this.size;
	}

	getRotation() {
		return this.getCurrentConfig().getRotation();
	}

	/**
	 * @param newSize {number}
	 */
	setSize(newSize) {
		this.version++;
		if (_.isUndefined(this.size)) {
			if (this.isStep()) {
				this.statusMap.forEach(statusMap => {
					statusMap.size = +newSize;
				});
			} else {
				this.getCurrentConfig().setSize(+newSize);
			}
		} else {
			this.size = +newSize;
		}
	}

	setRotation(angle) {
		this.getCurrentConfig().setRotation(+angle);
		this.version++;
	}

	/**
	 * @param sizeInPx {number}
	 * @param unit {number}
	 */
	calculateSize(sizeInPx, unit) {
		this.version++;
		if (unit === 0) {
			this.setSize(0);
		} else {
			this.setSize(+sizeInPx / +unit);
		}
	}

	getCurrentFrame() {
		const THIS = this;
		let foundedFrame = _.find(this.frames, frame => frame.getId() === THIS.getCurrentConfig().getFrame());
		if (_.isUndefined(foundedFrame)) foundedFrame = defaultFrame;
		return foundedFrame;
	}

	getFrameByStatus(status) {
		const THIS = this;
		let foundedFrame = _.find(this.frames, frame => frame.getId() === THIS.getConfigByIndex(status).getFrame());
		if (_.isUndefined(foundedFrame)) foundedFrame = defaultFrame;
		return foundedFrame;
	}

	setCurrentFrame(frameId) {
		this.getCurrentConfig().setFrame(+frameId);
	}

	setMapId(mapId) {
		this.version++;
		this.mapId = +mapId;
	}

	setName(name) {
		this.version++;
		this.name = '' + name;
	}

	/**
	 * @param id {number}
	 */
	setId(id) {
		this.version++;
		this.id = +id;
	}

	/**
	 * @param position {object}
	 */
	setPosition(position) {
		this.version++;
		if (this.isStep()) {
			this.statusMap.forEach(statusMap => {
				statusMap.setPosition(position);
			});
		}
		else {
			this.getCurrentConfig().setPosition(position);
		}
	}

	setLottieSettings(lottieSettings) {
		this.getCurrentConfig().setLottieSettings(lottieSettings);
		this.version++;
	}

	setFrames(frames) {
		this.version++;
		this.frames = frames;
	}

	getFrames() {
		return this.frames;
	}

	/**
	 * @returns {number}
	 */
	getId() {
		return +this.id;
	}

	/**
	 * @param index {number}
	 * @returns {File}
	 */
	getFrameByIndex(index) {
		return this.frames[+index];
	}

	/**
	 * @param frameId {number}
	 * @returns {File}
	 */
	getFrameById(frameId) {
		return _.find(this.frames, frame => frame.getId() === frameId);
	}

	/**
	 * @returns {string}
	 */
	getName() {
		return '' + this.name;
	}

	/**
	 * @returns {number}
	 */
	getCategory() {
		return +this.categoryId;
	}

	/**
	 * @returns {number}
	 */
	getMapId() {
		return +this.mapId;
	}

	getCurrentConfig() {
		return this.statusMap[this.status];
	}

	getConfigByIndex(index) {
		return this.statusMap[index];
	}

	getListenerById(id) {
		return _.find(this.listeners, listener => listener.getId() === +id);
	}

	getListeners() {
		return this.listeners;
	}

	/**
	 * @returns {object}
	 */
	getPosition() {
		return this.getCurrentConfig().getPosition();
	}

	getLottieSettings() {
		return this.getCurrentConfig().getLottieSettings();
	}

	makeRawObject() {
		let data = {
			id: this.id,
			name: this.name,
			categoryId: this.categoryId,
			listeners: this.listeners.map(listener => listener.getRawObject()),
			frames: _.clone(this.frames),
			mapId: this.mapId,
			status: this.status,
			statusMap: this.statusMap.map(status => status.makeRawObject()),
			type: this.getType(),
			additionalInfo: _.cloneDeep(this.getAdditionalInfo()),
			transitions: this.transitions.map(transition => transition.makeRawObject())
		};
		if (!_.isUndefined(this.size)) data.size = this.size;
		return data;
	}

	static copyItem(originalItem) {
		let originalRaw = originalItem.makeRawObject();
		return Item.parse(originalRaw);
	}

	static copyItemFromCategory(originalItem) {
		let originalRaw = originalItem.makeRawObject();
		originalRaw.statusMap[0].size = originalRaw.size;
		if (originalRaw.statusMap[1]) originalRaw.statusMap[1].size = originalRaw.size;
		if (originalRaw.statusMap[2]) originalRaw.statusMap[2].size = originalRaw.size;
		delete originalRaw.size;
		return Item.parse(originalRaw);
	}

	/**
	 * @param iterator {function}
	 * @return {any[]}
	 */
	framesMap(iterator) {
		return this.frames.map(iterator);
	}

	updateListenersVersion() {
		this.listenersVersion++;
	}

	updateListener(newListener) {
		this.listenersVersion++;
		this.listeners = this.listeners.map(listener => {
			if (listener.getId() === newListener.getId()) return newListener;
			return listener;
		});
	}

	addListener(newListener) {
		this.listenersVersion++;
		this.listeners.unshift(newListener);
	}

	/**
	 * Delete listener from item.
	 *
	 * @param id {number}
	 */
	removeListener(id) {
		this.listenersVersion++;
		this.listeners = _.filter(this.listeners, listener => listener.id !== +id);
	}

	/**
	 * Remove frames from item.
	 *
	 * @param id {number};
	 */
	deleteFrame(id) {
		let frames = _.filter(this.frames, frame => frame.id !== id);
		_.forEach(this.statusMap, statusMap => {
			if (statusMap.getFrameByIndex() === id) {
				statusMap.deleteCurrentFrame();
			}
		});

		this.setFrames(frames);

	}

	/**
	 * Find new frame after or before given.
	 * First choose next, if not exits - choose previous.
	 *
	 * @param id {number}
	 * @return {*}
	 */
	getNewFrame(id) {
		let frame = {};

		if (!_.isEmpty(frame = this.getNextFrame(id))) {
			return frame;
		}
		else if (!_.isEmpty(frame = this.getPreviousFrame(id))) {
			return frame;
		}

		return frame;
	}

	/**
	 * Get next frame after given
	 * @param id {number}
	 * @return {*}
	 */
	getNextFrame(id) {
		let frame = {};
		let framesCount = this.frames.length;
		let framePosition = _.findIndex(this.frames, {id});

		if (framePosition > 0 && (framesCount - 1) !== framePosition) {
			frame = this.frames[framePosition + 1];
		}

		return frame;
	}

	/**
	 * Get previous frame after given.
	 * @param id {number}
	 * @return {*}
	 */
	getPreviousFrame(id) {
		let frame = {};
		let framePosition = _.findIndex(this.frames, {id});

		if (framePosition > 0) {
			frame = this.frames[framePosition - 1];
		}

		return frame;
	}

	/**
	 * Update item version up.
	 */
	versionUp() {
		this.version++;
	}

}