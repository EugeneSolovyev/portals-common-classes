import EventFilter from './EventFilter';
import EventAction from './EventAction';
import Store from './Store';

import {bindThis} from 'Common';

export default class Listener extends Store{
	/**
	 * @class
	 * @param id {number}
	 * @param eventType {number}
	 * @param eventFilter {EventFilter}
	 * @param eventAction {EventAction}
	 * @param once {boolean}
	 * @param done {boolean}
	 * @param eventTryNumber {number}
	 * @param additionalInfo {JSON}
	 */
	constructor(id, eventType, eventFilter, eventAction, once, done, eventTryNumber, additionalInfo) {
		super();

		this.id = id;
		this.eventType = eventType;
		this.eventFilter = eventFilter;
		this.eventAction = eventAction;
		this.once = once;
		this.done = done;
		this.eventTryNumber = eventTryNumber;
		this.additionalInfo = additionalInfo;

		this.version = 0;

		bindThis([
			this.getId,
			this.getEventType,
			this.getEventFilter,
			this.getEventTryNumber,
			this.isDone,
			this.isOnce,
			this.setEventType,
			this.setEventTryNumber,
			this.updateEventTryNumber,
			this.toggleIsOnce,
			this.toggleIsOnceWithUpdate,
			this.updateEventFilter,
			this.setEventFilter,
			this.updateEventType,
			this.updateEventAction,
			this.setEventAction,
			this.getEventAction,
			this.getAdditionalInfo,
			this.handleTheEvent,
			this.clone,
			this.isStepListener,
			this.versionUp,
		], this);
	}

	static getDefaultListener() {
		return Listener.parse({
			id: Date.now(),
			eventType: ENUMS.EVENT_TYPES.SCORE_CHANGED.ID,
			eventFilter: EventFilter.getDefaultFilter(),
			eventAction: EventAction.getDefaultAction(),
			once: false,
			done: false,
			eventTryNumber: 1,
		});
	}

	/**
	 *
	 * @param rawObject {Object}
	 * @returns {Listener}
	 */
	static parse(rawObject) {
		let id = +rawObject.id;
		let eventType = +rawObject.eventType;
		let eventFilter = EventFilter.parse(rawObject.eventFilter);
		let eventAction = EventAction.parse(rawObject.eventAction);
		let once = !!rawObject.once;
		let done = !!rawObject.done;
		let eventTryNumber = +rawObject.eventTryNumber;
		let additionalInfo = rawObject.additionalInfo || {};
		return new Listener(id, eventType, eventFilter, eventAction, once, done, eventTryNumber, additionalInfo);
	}

	versionUp() {
		this.version++;
		super.Store.dispatch({type: ACTION_TYPES.UPDATE_LISTENER});
	}

	isStepListener() {
		return this.getAdditionalInfo().isStepListener;
	}

	/**
	 * @param value {number}
	 */
	setEventTryNumber(value) {
		this.eventTryNumber = +value;
	}

	updateEventTryNumber(value) {
		this.setEventTryNumber(value);
		this.versionUp();
	}

	getEventTryNumber() {
		return this.eventTryNumber;
	}

	clone() {
		let eventFilter = this.eventFilter.clone();
		let eventAction = this.eventAction.clone();

		return new Listener(this.id, this.eventType, eventFilter, eventAction, this.once, this.done, this.eventTryNumber,
			this.getAdditionalInfo());
	}

	setEventType(eventType) {
		this.eventType = eventType;
	}

	isDone() {
		return this.done;
	}

	isOnce() {
		return this.once;
	}

	toggleIsOnce() {
		this.once = !this.once;
	}

	toggleIsOnceWithUpdate() {
		this.toggleIsOnce();
		this.versionUp();
	}

	getRawObject() {
		return {
			eventType: this.eventType,
			eventFilter: this.eventFilter.getRawObject(),
			eventAction: this.eventAction.getRawObject(),
			once: this.once,
			done: this.done,
			eventTryNumber: this.eventTryNumber,
			additionalInfo: _.cloneDeep(this.getAdditionalInfo()),
		};
	}

	getAdditionalInfo() {
		return this.additionalInfo;
	}

	getId() {
		return this.id;
	}

	getEventFilter() {
		return this.eventFilter;
	}

	getEventType() {
		return this.eventType;
	}

	getEventAction() {
		return this.eventAction;
	}

	setEventFilter(breadcrumbs, data) {
		let targetFilter = breadcrumbs.reduce((sum, current) => {
			return sum.value[current];
		}, this.eventFilter);
		targetFilter.modify(data);
	}

	updateEventFilter(breadcrumbs, data) {
		this.setEventFilter(breadcrumbs, data);
		this.versionUp();
	}

	updateEventType(value, parentItemId) {
		this.setEventType(value);
		if (this.getEventType() === ENUMS.EVENT_TYPES.CLICK_ON_ITEM.ID) {
			this.setEventFilter([], {
				name: 'value',
				value: [parentItemId]
			});
			this.setEventFilter([], {
				name: 'operation',
				value: ENUMS.FILTER_OPERATIONS.EQUAL
			});
		}

		this.versionUp();
	}

	setEventAction(breadcrumbs, data) {
		let targetAction = breadcrumbs.reduce((sum, current) => {
			return sum.value[current];
		}, this.eventAction);
		targetAction.modify(data);
	}

	updateEventAction(breadcrumbs, data) {
		this.setEventAction(breadcrumbs, data);
		this.versionUp();
	}

	handleTheEvent(eventType, values, item, projectEventHandler, transitionChain, newTransitionContainer) {
		if (this.eventType !== eventType) return new Promise((res, rej) => res());
		if (this.eventFilter.checkYourSelf(values, this.eventType)) {
			return this.eventAction.runYourSelf(item, projectEventHandler, transitionChain, newTransitionContainer);
		}
		else {
			return new Promise((res, rej) => res());
		}
	}
}