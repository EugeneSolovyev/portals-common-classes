import Item from './Item';
import File from './File';
import {bindThis} from 'Common';

export default class Map {
	/**
	 * @class
	 * @param id {number}
	 * @param name {string}
	 * @param background {File}
	 * @param projectId {number}
	 * @param status {number}
	 * @param settings {object} - {orientation: number, direction: number}
	 * @param items {Item[]}
	 */
	constructor(id, name, background, projectId, status, settings, items) {
		/** @private */ this.id = id;
		/** @private */ this.name = name;
		/** @private */ this.background = background;
		/** @private */ this.projectId = projectId;
		/** @private */ this.status = status;
		/** @private */ this.settings = settings;
		/** @private */ this.items = items;
		this.version = 0;
		this.itemsVersion = 0;

		bindThis([
			this.getId,
			this.addItem,
			this.getOrientation,
			this.getBackground,
			this.getItemById,
			this.getItems,
			this.getName,
			this.itemsMap,
			this.setName,
			this.setBackground,
			this.setStatus,
			this.setSettings,
			this.deleteItem,
			this.handleTheEvent,
			this.isVertical,
			this.hasStepWithNumber,
			this.getLastStep,
			this.getDirection,
			this.getStepByNumber,
			this.getOverscroll,
			this.getFirstStep,
		], this);
	}

	static parse(rawObject) {
		let id = rawObject.id;
		let name = '' + rawObject.name;
		let background = File.parse(rawObject.background || {});
		let projectId = rawObject.projectId;
		let status = +rawObject.status;
		let settings = {
			orientation: _.get(rawObject, 'settings.orientation', ENUMS.MAP_ORIENTATION.VERTICAL),
			direction: _.get(rawObject, 'settings.direction', ENUMS.MAP_DIRECTION.FORWARD),
			overscroll: _.get(rawObject, 'settings.overscroll', 2),
			unscrollable: !!_.get(rawObject, 'settings.unscrollable', false),
		};
		let items = (rawObject.mapItems || []).map(item => Item.parse(item));
		return new Map(id, name, background, projectId, status, settings, items);
	}

	static mergeChanges(targetMap, changes) {
		if (!_.isUndefined(changes.name)) targetMap.setName(changes.name);
		if (!_.isUndefined(changes.background)) targetMap.setBackground(changes.background);
		if (!_.isUndefined(changes.status)) targetMap.setStatus(changes.status);
		if (!_.isUndefined(changes.settings)) targetMap.setSettings(changes.settings);
	}

	/**
	 * @param eventType {ENUMS.EVENT_TYPES}
	 * @param projectEventHandler {function}
	 * @param value {any}
	 * @param transitionChain -{tipa_Promise}
	 */
	handleTheEvent(eventType, value, projectEventHandler, transitionChain) {
		_.forEach(this.items,
			item => item.handleTheEvent(eventType, value, projectEventHandler, () => {this.itemsVersion++;}, transitionChain));
	}

	getLastStep() {
		return _.max(this.getItems().map(item => item.getStep())) || 0;
	}

	getFirstStep() {
		return _.min(this.getItems().map(item => item.getStep())) || 0;
	}

	getStepByNumber(step) {
		let result = _.find(this.getItems(), item => item.getStep() === step);
		if (_.isUndefined(result)) {
			let lastStep = this.getLastStep();
			if (lastStep < step) return this.getStepByNumber(lastStep);
			let firstStep = this.getFirstStep();
			if (firstStep > step) return this.getStepByNumber(firstStep);
		}
		return result;
	}

	hasStepWithNumber(number) {
		return ~_.findIndex(this.getItems(), item => item.getStep() === number);
	}

	setName(name) {
		this.version++;
		this.name = '' + name;
	}

	setBackground(background) {
		this.version++;
		this.background = File.parse(background);
	}

	setStatus(status) {
		this.version++;
		this.status = +status;
	}

	setSettings(settings) {
		this.version++;
		this.settings = {
			orientation: _.get(settings, 'orientation', ENUMS.MAP_ORIENTATION.VERTICAL),
			direction: _.get(settings, 'direction', ENUMS.MAP_DIRECTION.FORWARD),
			overscroll: _.get(settings, 'overscroll', 2),
			unscrollable: _.get(settings, 'unscrollable', false),
		};
	}

	getSettings() {
		return _.cloneDeep(this.settings);
	}

	getName() {
		return this.name;
	}

	getBackground() {
		return this.background;
	}

	getId() {
		return this.id;
	}

	getItemById(itemId) {
		return _.find(this.items, item => item.getId() === itemId);
	}

	getItems() {
		return this.items;
	}

	/**
	 * @param newItem {Item}
	 */
	addItem(newItem) {
		this.itemsVersion++;
		this.items.push(newItem);
	}

	/**
	 * @return {number}
	 */
	getOrientation() {
		return this.settings.orientation;
	}

	getDirection() {
		return this.settings.direction;
	}

	getOverscroll() {
		return +this.settings.overscroll;
	}

	getUnscrollable() {
		return this.settings.unscrollable;
	}

	isVertical() {
		return this.getOrientation() === ENUMS.MAP_ORIENTATION.VERTICAL;
	}

	/**
	 *
	 * @param iterator {function}
	 * @return {any[]}
	 */
	itemsMap(iterator) {
		return this.items.map(iterator);
	}

	deleteItem(id) {
		this.itemsVersion++;
		this.items = this.items.filter(item => item.id !== id);
	}
}