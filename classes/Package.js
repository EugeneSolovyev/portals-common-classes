import Category from './Category';

export default class Package {
	/**
	 * @class Package
	 * @param id {number}
	 * @param name {string}
	 * @param categories {Category[]}
	 * @param image {string}
	 */
	constructor(id, name, categories, image) {
		this.id = id;
		this.name = name;
		this.categories = categories;
		this.image = image;

		this.version = 0;

		this.categoryMap = this.categoryMap.bind(this);
		this.getId = this.getId.bind(this);
		this.addItem = this.addItem.bind(this);
		this.findItemByIdAndCategory = this.findItemByIdAndCategory.bind(this);
		this.getCategoryById = this.getCategoryById.bind(this);
		this.addCategory = this.addCategory.bind(this);
		this.setName = this.setName.bind(this);
		this.getName = this.getName.bind(this);
		this.setId = this.setId.bind(this);
		this.makeRawObject = this.makeRawObject.bind(this);
		this.setImage = this.setImage.bind(this);
		this.getImage = this.getImage.bind(this);
		this.clone = this.clone.bind(this);
		this.removeItem = this.removeItem.bind(this);
		this.removeCategory = this.removeCategory.bind(this);
	}

	/**
	 * @param rawObject {object}
	 * @returns {Package}
	 */
	static parse(rawObject) {
		let id = +rawObject.id;
		let name = '' + rawObject.name;
		let categories = (rawObject.categories || []).map(category => Category.parse(category));
		let image = '' + rawObject.image;
		return new Package(id, name, categories, image);
	}

	static mergeChanges(targetPackage, changes) {
		if (!_.isUndefined(changes.name)) targetPackage.setName(changes.name);
		if (!_.isUndefined(changes.image)) targetPackage.setImage(changes.image);
	}

	/**
	 *
	 * @param categoryLink {Category}
	 * @param changes {object}
	 */
	updateCategory(categoryLink, changes) {
		Category.mergeChanges(categoryLink, changes);
		this.version++;
	}

	removeItem(item) {
		this.getCategoryById(item.getCategory()).removeItem(item);
		this.version++;
	}

	clone() {
		return new Package(this.id, this.name, this.categories, this.image);
	}

	setImage(image) {
		this.version++;
		this.image = '' + image;
	}

	getImage() {
		return URLES.ServerURL + this.image;
	}

	makeRawObject() {
		return {
			id: this.id,
			name: this.name,
			image: this.image,
		}
	}

	setName(name) {
		this.version++;
		this.name = '' + name;
	}

	getName() {
		return this.name;
	}

	setId(id) {
		this.version++;
		this.id = +id;
	}

	categoryMap(callback) {
		return this.categories.map(callback);
	}

	getId() {
		return this.id;
	}

	/**
	 * @param newItem {Item}
	 */
	addItem(newItem) {
		let category = _.find(this.categories, category => category.getId() == newItem.getCategory());
		if (category) category.addItem(newItem);
		this.version++;
	}

	/**
	 *
	 * @param categoryId {number}
	 * @return {Category|null}
	 */
	getCategoryById(categoryId) {
		return _.find(this.categories, category => category.getId() === categoryId) || null;
	}

	/**
	 * @param itemId {number}
	 * @param categoryId {number}
	 * @return {Item|null}
	 */
	findItemByIdAndCategory(itemId, categoryId) {
		const targetCategory = this.getCategoryById(categoryId);
		if (!targetCategory) return null;
		return targetCategory.getItemById(itemId);
	}

	addCategory(newCategory) {
		this.version++;
		this.categories.unshift(newCategory);
	}

	removeCategory(category) {
		this.version++;
		this.categories = this.categories.filter(sourceCategory => sourceCategory.getId() !== category.getId());
	}
}