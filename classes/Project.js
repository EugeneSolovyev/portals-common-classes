import moment from 'moment';
import Map from './Map';
import {bindThis} from 'Common';
import Backinator from 'Backinator';
import File from './File';
import Store from './Store';

const defaultFrame = File.parse({});
const MS_IN_DAY = 86400000;
const SCORE_DATE_FORMAT = 'YYYY-MM-DD';

export default class Project extends Store{
	/**
	 * @class
	 * @param id {number}
	 * @param name {string}
	 * @param settings {object} - {mapsOrder: number[]}
	 * @param type {number}
	 * @param maps {Map[]}
	 * @param image {string}
	 * @param scoring {number}
	 * @param tags {Array}
	 * @param flags {Object[]}
	 * @param status {number}
	 * @param dates {Object}
	 * @param schedule {Object[]}
	 */
	constructor(id, name, settings, type, maps, image, scoring, tags, flags, status, dates, schedule) {
		super();

		/** @private */ this.id = id;
		/** @private */ this.name = name;
		/** @private */ this.settings = settings;
		/** @private */ this.type = type;
		/** @private */ this.maps = maps;
		/** @private */ this.image = image;
		/** @private */ this.scoring = scoring;
		/** @private */ this.tags = tags;
		/** @private */ this.flags = flags;
		/** @private */ this.status = status;
		/** @private */ this.dates = dates;
		/** @private */ this.schedule = schedule;

		this.version = 0;

		bindThis([
			this.getName,
			this.addItemToMap,
			this.getId,
			this.getMapById,
			this.getMapByIndex,
			this.getMapsCount,
			this.getMapIdByIndex,
			this.getMaps,
			this.addMap,
			this.deleteMap,
			this.getSettings,
			this.setId,
			this.setName,
			this.setSettings,
			this.setType,
			this.setScore,
			this.getScore,
			this.getStatus,
			this.setStatus,
			this.getStepsCount,
			this.getType,
			this.getMapIndex,
			this.makeRawObject,
			this.getImage,
			this.hasMap,
			this.toggleEmulating,
			this.handleTheEvent,
			this.checkSettings,
			this.getContents,
			this.getContentById,
			this.addScore,
			this.getOverviewPicture,
			this.getOverview,
			this.getTags,
			this.setTags,
			this.getAuthor,
			this.getActiveMap,
			this.versionUp,
			this.getFlags,
			this.setFlags,
			this.getCreationDate,
			this.getUpdateDate,
			this.makeInitialEvents,
			this.getSchedule,
			this.setSchedule,
			this.isTodayProject,
			this.getDayStrike,
			this.isDoneToday,
			this.getScoreForMonth,
		], this);
	}

	/**
	 *
	 * @param rawObject {object}
	 * @returns {Project}
	 */
	static parse(rawObject) {
		let id = +rawObject.id;
		let name = '' + rawObject.name;
		let settings = {
			mapsOrder: _.get(rawObject, 'settings.mapsOrder', []),
			stepImage: _.get(rawObject, 'settings.stepImage', []),
			scheduleOn: _.get(rawObject, 'settings.scheduleOn', false),
		};
		let type = +rawObject.type;
		let maps = _.get(rawObject, 'maps', []).map(map => Map.parse(map));
		let image = '' + rawObject.image;
		let scoring = {
			current: +(rawObject.totalScoring || 0),
			total: +rawObject.stepsTotal,
		};
		let tags = rawObject.tags || [];
		let flags = rawObject.flags || [];
		let status = +rawObject.status || ENUMS.PROJECT_STATUS.NEW;
		let dates = {
			createdAt: moment(rawObject.createdAt, moment.ISO_8601),
			lastUpdated: moment(_.get(rawObject.scoring, '[0].createdAt', rawObject.createdAt), SCORE_DATE_FORMAT),
			dayStrike: rawObject.scoring ? Project.calculateDayStrike(rawObject.scoring) : 0,
			lastMonthScores: Project.calculateScoreByMonth(rawObject.scoring),
		};
		let schedule = JSON.parse(_.get(rawObject.schedule, 'value', '[]'));
		return new Project(id, name, settings, type, maps, image, scoring, tags, flags, status, dates, schedule);
	}

	static calculateDayStrike(scores) {
		if (!scores.length) return 0;
		let today = moment().startOf('day');
		let i = 0;
		let dayStrikes = 0;
		let nextScore;
		do {
			nextScore = moment(scores[i].createdAt, SCORE_DATE_FORMAT);
			if (today.diff(nextScore) === MS_IN_DAY) dayStrikes += 1;
			i += 1;
			today = nextScore;
		} while (i < scores.length && today.diff(nextScore) <= MS_IN_DAY);
		return dayStrikes;
	}

	static calculateScoreByMonth(scores) {
		if (!scores) return 0;
		let thisMonth = moment().month();
		return scores
			.map(score => moment(score.createdAt, SCORE_DATE_FORMAT).month())
			.reduce((acc, date) => {
				if (date === thisMonth) acc++;
				return acc;
			}, 0);
	}

	getScoreForMonth() {
		return this.dates.lastMonthScores;
	}

	getDayStrike() {
		return this.dates.dayStrike;
	}

	isTodayProject() {
		let today = moment().isoWeekday();
		return _.findIndex(this.getSchedule(), rule => !_.isUndefined(rule.dayInWeek) && rule.dayInWeek === today) !== -1;
	}

	isDoneToday() {
		return this.getUpdateDate().isSame(moment(), 'day');
	}

	makeInitialEvents() {
		this.handleTheEvent(ENUMS.EVENT_TYPES.SCORE_CHANGED.ID, [this.getScore()]);
	}

	getCreationDate() {
		return this.dates.createdAt;
	}

	getUpdateDate() {
		return this.dates.lastUpdated;
	}

	/**
	 * @param eventType {ENUMS.EVENT_TYPES}
	 * @param value {Array}
	 */
	handleTheEvent(eventType, value, transitionChain = Promise.resolve()) {
		_.forEach(this.maps, map => map.handleTheEvent(eventType, value, this.handleTheEvent, transitionChain));
	}

	addScore(value, eventInfo) {
		let promise = eventInfo.isInitialEvent || !value
						  ? Promise.resolve(this.getScore())
						  : Backinator.setProjectScore(this.getId(), value, eventInfo);
		let THIS = this;

		//if (!eventInfo.isInitialEvent && THIS.getScore() === res) return;
		this.setScore(this.getScore() + value);
		this.version++;
		setTimeout(() => {
			_.forEach(THIS.maps, map =>
				map.handleTheEvent(ENUMS.EVENT_TYPES.SCORE_CHANGED.ID, [THIS.getScore()], THIS.handleTheEvent));
			super.Store.dispatch({
				type: ACTION_TYPES.USER_STATISTICS_ADD_SCORE,
				payload: 1
			});
		}, 0);
	}

	/**
	 * @param value {number}
	 */
	setScore(value) {
		this.scoring.current = +value;
	}

	getActiveMap() {
		let currentStep = this.getScore() + 1;
		let index = _.findIndex(this.getSettings().mapsOrder.map(mapId => this.getMapById(mapId)),
			map => map.hasStepWithNumber(currentStep));
		if (index === -1) {
			if (this.getMaps().every(map => map.getLastStep() < currentStep)) {
				index = this.getMaps().length - 1;
			} else {
				index = 0;
			}
		}
		return index;
	}

	getScore() {
		return this.scoring.current;
	}

	getStatus() {
		return this.status;
	}

	setStatus(status) {
		this.status = +status;
	}

	getStepsCount() {
		return this.scoring.total;
	}

	toggleEmulating() {
		this.version++;
		this.isEmulating = !this.isEmulating;
	}

	hasMap() {
		return this.getMapsCount() > 0;
	}

	setId(id) {
		this.version++;
		this.id = +id;
	}

	setName(name) {
		this.version++;
		this.name = '' + name;
	}

	setSettings(settings) {
		this.version++;
		this.settings = {
			mapsOrder: _.get(settings, 'mapsOrder', this.getSettings().mapsOrder || []),
			scheduleOn: _.get(settings, 'scheduleOn', this.getSettings().scheduleOn || false),
			stepImage: _.get(settings, 'stepImage', this.getSettings().stepImage || [])
		};
	}

	getSchedule() {
		return this.schedule;
	}

	setSchedule(schedule) {
		this.schedule = schedule;
	}

	setType(type) {
		this.version++;
		this.type = +type;
	}

	getType() {
		return this.type;
	}

	getAuthor() {
		return 'Chukcha Pisatel';
	}

	getTags() {
		return this.tags;
	}

	getFlags() {
		return this.flags;
	}

	setFlags(flags) {
		this.flags = flags;
	}

	setTags(tags) {
		this.tags = tags;
	}

	getOverview() {
		return 'Alalfsd asdrgsadfgs serg dfgsddf hrehtter thertherther thsdhsdfh sdgf sdfgsdfgsdfg sdfg sdf gsdfgsdfg';
	}

	makeRawObject() {
		return {
			id: this.id,
			name: this.name,
			settings: this.settings,
			type: this.type,
			scoring: this.scoring,
			flags: this.flags,
			tags: this.tags,
			status: this.status,
		};
	}

	/**
	 * @returns {string}
	 */
	getImage() {
		return URLES.ServerURL + this.image;
	}

	getOverviewPicture() {
		return defaultFrame.getValue();
	}

	static mergeChanges(targetProject, changes) {
		if (!_.isUndefined(changes.name)) targetProject.setName(changes.name);
		if (!_.isUndefined(changes.settings)) targetProject.setSettings(changes.settings);
		if (!_.isUndefined(changes.schedule)) targetProject.setSchedule(changes.schedule);
		if (!_.isUndefined(changes.type)) targetProject.setType(changes.type);
		if (!_.isUndefined(changes.tags)) targetProject.setTags(changes.tags);
		if (!_.isUndefined(changes.flags)) targetProject.setFlags(changes.flags);
	}

	addMap(newMap) {
		this.maps.push(newMap);
		this.version++;
	}

	deleteMap(mapId) {
		this.version++;
		this.maps = this.maps.filter(map => map.id !== mapId);
		this.settings.mapsOrder = this.settings.mapsOrder.filter(id => id !== mapId);
	}

	getName() {
		return this.name;
	}

	/**
	 * @returns {number}
	 */
	getId() {
		return this.id;
	}

	/**
	 *
	 * @return {Map[]}
	 */
	getMaps() {
		return this.maps;
	}

	/**
	 * @param mapId {number}
	 * @param item {Item}
	 */
	addItemToMap(mapId, item) {
		let targetMap = this.getMapById(mapId);
		targetMap.addItem(item);
	}

	/**
	 * @param mapId {number}
	 * @return {Map|undefined}
	 */
	getMapById(mapId) {
		return _.find(this.maps, map => map.getId() === mapId);
	}

	/**
	 * @param index {number}
	 * @return {Map|undefined}
	 */
	getMapByIndex(index) {
		let id = this.settings.mapsOrder[index];
		return this.getMapById(id);
	}

	/**
	 *
	 * @param index {number}
	 * @return {number}
	 */
	getMapIdByIndex(index) {
		return +this.settings.mapsOrder[index];
	}

	getSettings() {
		return this.settings;
	}

	getMapsCount() {
		return this.settings.mapsOrder.length;
	}

	getMapIndex(mapId) {
		return +_.findKey(this.settings.mapsOrder, (id) => id === mapId);
	}

	getItems() {
		let items = [];
		_.forEach(this.getMaps(), map => {
			items = items.concat(map.getItems());
		});
		return items;
	}

	getContents() {
		let contents = [];

		_.forEach(this.getItems(), item => {
			contents = contents.concat(item.getContent());
		});

		return contents;
	}

	getContentById(id) {
		return this.getContents().find(content => content.getId() === id);
	}

	checkSettings(etalonSettings) {
		let result = true;
		_.forEach(etalonSettings, (value, key) => {
			result = result && _.isEqual(this.getSettings()[key], value);
		});
		return result;
	}

	versionUp() {
		this.version++;
	}
}