import {bindThis} from 'Common';

export default class StatusMap {

	/**
	 * @class
	 * @param currentFrame {number}
	 * @param position {object}
	 * @param size {number}
	 * @param rotation {number}
	 * @param fontSize {number}
	 * @param fontWeight {number}
	 * @param fontColor {string}
	 * @param fontPosition {object}
	 * @param lottieSettings {object}
	 */
	constructor(currentFrame, position, size, rotation, fontSize, fontWeight, fontColor, fontPosition, lottieSettings) {
		this.currentFrame = currentFrame;
		this.position = position;
		this.size = size;
		this.fontSize = fontSize;
		this.fontWeight = fontWeight;
		this.fontColor = fontColor;
		this.fontPosition = fontPosition;
		this.rotation = rotation;
		this.lottieSettings = lottieSettings;

		bindThis([
			this.clone,
			this.makeRawObject,
			this.getFrame,
			this.setFrame,
			this.setLottieSettings,
			this.getLottieSettings,
			this.setPosition,
			this.getPosition,
			this.getRotation,
			this.setRotation,
			this.setSize,
			this.getSize,
			this.setFontSize,
			this.getFontSize,
			this.setFontColor,
			this.getFontColor,
			this.setFontWeight,
			this.getFontWeight,
			this.getFontPosition,
			this.setFontPosition,
			this.setFontPositionX,
			this.setFontPositionY,
		], this);
	}

	static parse(rawObject) {
		let size = +rawObject.size || 0.05;
		let position = {
			x: +_.get(rawObject, 'position.x', 0),
			y: +_.get(rawObject, 'position.y', 0),
			z: _.min([+_.get(rawObject, 'position.z', 0), ENUMS.ITEM_RESTRICTIONS.MAX_Z]),
		};
		let currentFrame = +rawObject.currentFrame || 0;
		let fontSize = +(rawObject.fontSize || 0);
		let fontWeight = +(rawObject.fontWeight || 0);
		let fontColor = '' + (rawObject.fontColor || '#000000');
		let fontPosition = {
			x: +(_.get(rawObject, 'fontPosition.x', 0)),
			y: +(_.get(rawObject, 'fontPosition.y', 0)),
		};
		let rotation = +(rawObject.rotation || 0);
		let lottieSettings = {
			idleStart: +(_.get(rawObject.lottieSettings, 'idleStart', 0)),
			idleEnd: +(_.get(rawObject.lottieSettings, 'idleEnd', 0)),
			clickStart: +(_.get(rawObject.lottieSettings, 'clickStart', 0)),
			clickEnd: +(_.get(rawObject.lottieSettings, 'clickEnd', 0)),
		};
		return new StatusMap(currentFrame, position, size, rotation, fontSize, fontWeight, fontColor, fontPosition,
			lottieSettings);
	}

	getFontPosition() {
		return this.fontPosition;
	}

	setFontPosition({x = this.getFontPosition().x || 0, y = this.getFontPosition().y || 0}) {
		this.fontPosition = {
			x: +x,
			y: +y,
		};
	}
	setFontPositionX(x) {
		if (_.isUndefined(x)) return;
		this.fontPosition.x = x;
	}

	setFontPositionY(y) {
		if (_.isUndefined(y)) return;
		this.fontPosition.y = y;
	}

	setFontWeight(fontWeight) {
		this.fontWeight = fontWeight;
	}

	getFontWeight() {
		return this.fontWeight || 500;
	}

	setFontColor(fontColor) {
		this.fontColor = fontColor;
	}

	getFontColor() {
		return this.fontColor || '#ffffff';
	}

	setFontSize(fontSize) {
		this.fontSize = fontSize;
	}

	getFontSize() {
		return this.fontSize;
	}

	setSize(size) {
		this.size = size;
	}

	getSize() {
		return this.size;
	}

	getRotation() {
		return this.rotation;
	}

	setRotation(angle) {
		this.rotation = angle;
	}

	setPosition({x = this.getPosition().x || 0, y = this.getPosition().y || 0, z = this.getPosition().z || 0}) {
		this.position = {
			x,
			y,
			z
		};
	}

	getPosition() {
		return this.position;
	}

	setLottieSettings(lottieSettings) {
		this.lottieSettings = lottieSettings;
	}

	getLottieSettings() {
		return this.lottieSettings;
	}

	getFrame() {
		return this.currentFrame;
	}

	setFrame(id) {
		this.currentFrame = id;
	}

	deleteCurrentFrame() {
		this.currentFrame = 0;
	}

	clone() {
		let position = _.clone(this.position);
		let fontPosition = _.clone(this.fontPosition);
		let lottieSettings = _.clone(this.lottieSettings);
		return new StatusMap(this.currentFrame, position, this.size, this.rotation, this.fontSize, this.fontWeight,
			this.fontColor, fontPosition, lottieSettings);
	}

	makeRawObject() {
		return {
			currentFrame: this.currentFrame,
			position: this.position,
			size: this.size,
			fontSize: this.fontSize,
			fontWeight: this.fontWeight,
			fontColor: this.fontColor,
			fontPosition: this.fontPosition,
			rotation: this.rotation,
			lottieSettings: this.lottieSettings,
		};
	}
}