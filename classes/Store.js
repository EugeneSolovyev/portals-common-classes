let Store;

class ProjectStore {
	get Store () {
		return Store;
	}

	static bindStore (externalStore) {
		Store = externalStore;
	}
}

export default ProjectStore;