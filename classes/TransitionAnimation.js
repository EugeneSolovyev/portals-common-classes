import Snap from 'snapsvg';

const ANIMATION_ACTION_FIELD_TYPES = {
	TEXT: 0,
	NUMBER: 1,
};

const ANIMATION_ACTION_TYPES = {
	SET_FRAME: 0,
	SCALE: 1,
	MOVE: 2,
	ROTATE: 3,
	OPACITY: 4,
	SVG_FILTER: 5,
	LOTTIE_PLAY: 6,
	SCALE_FONT: 7,
	FONT_COLOR: 8,
};

const ANIMATION_ACTION_TYPES_INVERTED = _.invert(ANIMATION_ACTION_TYPES);

const ANIMATION_ACTION_FIELDS = {
	[ANIMATION_ACTION_TYPES.SET_FRAME]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.SET_FRAME
		},
		frameId: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 0
		},
	},
	[ANIMATION_ACTION_TYPES.SCALE]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.SCALE
		},
		from: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		to: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		delay: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		duration: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		timing: {
			type: ANIMATION_ACTION_FIELD_TYPES.TEXT,
			default: 'linear'
		},
	},
	[ANIMATION_ACTION_TYPES.MOVE]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.MOVE
		},
		xFrom: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		yFrom: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		xTo: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		yTo: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		delay: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		duration: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		timing: {
			type: ANIMATION_ACTION_FIELD_TYPES.TEXT,
			default: 'linear'
		},
	},
	[ANIMATION_ACTION_TYPES.ROTATE]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.ROTATE
		},
		from: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		to: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		delay: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		duration: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		timing: {
			type: ANIMATION_ACTION_FIELD_TYPES.TEXT,
			default: 'linear'
		},
	},
	[ANIMATION_ACTION_TYPES.LOTTIE_PLAY]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.LOTTIE_PLAY
		},
		startFrame: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 0
		},
		endFrame: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 100
		},
	},
	[ANIMATION_ACTION_TYPES.SCALE_FONT]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.SCALE_FONT
		},
		from: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		to: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		delay: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		duration: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		timing: {
			type: ANIMATION_ACTION_FIELD_TYPES.TEXT,
			default: 'linear'
		},
	},
	[ANIMATION_ACTION_TYPES.FONT_COLOR]: {
		type: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: ANIMATION_ACTION_TYPES.FONT_COLOR
		},
		from: {
			type: ANIMATION_ACTION_FIELD_TYPES.STRING,
			default: '#ffffff'
		},
		to: {
			type: ANIMATION_ACTION_FIELD_TYPES.STRING,
			default: '#ffffff'
		},
		delay: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		duration: {
			type: ANIMATION_ACTION_FIELD_TYPES.NUMBER,
			default: 1
		},
		timing: {
			type: ANIMATION_ACTION_FIELD_TYPES.TEXT,
			default: 'linear'
		},
	},
};

import {bindThis} from 'Common';

export default class TransitionAnimation {

	constructor(animationActions, statusFrom, statusTo) {
		this.animationActions = animationActions;
		this.statusFrom = statusFrom;
		this.statusTo = statusTo;

		bindThis([
			this.setStatusTo,
			this.setStatusFrom,
			this.getAnimationBlocks,
			this.addAction,
			this.removeAction,
			this.updateAnimationAction,
			this.makeRawObject,
			this.play,
			this.doAction,
			this.doActionBlock,
			this.bindClassWithComponent,
		], this);
	}

	static parse(rawObject) {
		let animationActions = _.cloneDeep(rawObject.animationActions || []);
		let statusFrom = +(rawObject.statusFrom || 0);
		let statusTo = +(rawObject.statusTo || 0);
		return new TransitionAnimation(animationActions, statusFrom, statusTo);
	}

	static setValuesByStatusFrom(animationAction, statusMap) {
		switch (animationAction.type) {
			case ANIMATION_ACTION_TYPES.MOVE:
				animationAction.xFrom = statusMap.getPosition().x;
				animationAction.yFrom = statusMap.getPosition().y;
				break;
			case ANIMATION_ACTION_TYPES.SCALE:
				animationAction.from = statusMap.getSize();
				break;
			case ANIMATION_ACTION_TYPES.SCALE_FONT:
				animationAction.from = statusMap.getFontSize();
				break;
			case ANIMATION_ACTION_TYPES.FONT_COLOR:
				animationAction.from = statusMap.getFontColor();
				break;
			case ANIMATION_ACTION_TYPES.ROTATE:
				animationAction.from = statusMap.getRotation();
				break;
			case ANIMATION_ACTION_TYPES.LOTTIE_PLAY:
				animationAction.startFrame = statusMap.getLottieSettings().idleStart;
				break;
		}
	}

	static setValuesByStatusTo(animationAction, statusMap) {
		switch (animationAction.type) {
			case ANIMATION_ACTION_TYPES.SET_FRAME:
				animationAction.frameId = statusMap.getFrame();
				break;
			case ANIMATION_ACTION_TYPES.MOVE:
				animationAction.xTo = statusMap.getPosition().x;
				animationAction.yTo = statusMap.getPosition().y;
				break;
			case ANIMATION_ACTION_TYPES.SCALE:
				animationAction.to = statusMap.getSize();
				break;
			case ANIMATION_ACTION_TYPES.SCALE_FONT:
				animationAction.to = statusMap.getFontSize();
				break;
			case ANIMATION_ACTION_TYPES.FONT_COLOR:
				animationAction.to = statusMap.getFontColor();
				break;
			case ANIMATION_ACTION_TYPES.ROTATE:
				animationAction.to = statusMap.getRotation();
				break;
			case ANIMATION_ACTION_TYPES.LOTTIE_PLAY:
				animationAction.endFrame = statusMap.getLottieSettings().idleStart;
				break;
		}
	}

	setStatusTo(status, statusMap) {
		this.getAnimationBlocks()
			.forEach(animationBlock => animationBlock
				.forEach(animationAction => {
					TransitionAnimation.setValuesByStatusTo(animationAction, statusMap);
				}));
		this.statusTo = +status;
	}

	setStatusFrom(status, statusMap) {
		this.getAnimationBlocks()
			.forEach(animationBlock => animationBlock
				.forEach(animationAction => {
					TransitionAnimation.setValuesByStatusFrom(animationAction, statusMap);
				}));
		this.statusFrom = +status;
	}


	makeRawObject() {
		return {
			animationActions: this.animationActions,
			statusFrom: this.statusFrom,
			statusTo: this.statusTo,
		};
	}

	doActionBlock(actionBlock) {
		return Promise.all(actionBlock.map(action => this.doAction(action)));
	}

	doAction(action) {
		let THIS = this;
		return new Promise(makeActionDone => {
			let promise;
			switch (action.type) {
				case ANIMATION_ACTION_TYPES.SET_FRAME:
					promise = THIS.component.setFrame(action);
					break;
				case ANIMATION_ACTION_TYPES.MOVE:
					promise = THIS.component.moveTo(action);
					break;
				case ANIMATION_ACTION_TYPES.LOTTIE_PLAY:
					promise = THIS.component.playLottie(action);
					break;
				case ANIMATION_ACTION_TYPES.SCALE_FONT:
					promise = THIS.component.setFontScale(action);
					break;
				case ANIMATION_ACTION_TYPES.FONT_COLOR:
					promise = THIS.component.setFontColor(action);
					break;
				default:
					promise = Promise.resolve();
			}
			promise.then(makeActionDone);
		});
	}

	bindClassWithComponent(component) {
		this.component = component;
	}

	/**
	 *
	 * @return {Promise}
	 */
	play() {
		return this.getAnimationBlocks()
			.map(actionBlock => () => this.doActionBlock(actionBlock))
			.reduce((promiseChain, doCurrentActionBlock) => {
				return promiseChain.then(doCurrentActionBlock);
			}, Promise.resolve());
	}

	static get ANIMATION_ACTION_TYPES() {
		return ANIMATION_ACTION_TYPES;
	}

	static get ANIMATION_ACTION_TYPES_INVERTED() {
		return ANIMATION_ACTION_TYPES_INVERTED;
	}

	static createAction(actionType) {
		let result = {};
		_.forEach(ANIMATION_ACTION_FIELDS[actionType], (value, key) => {
			result[key] = value.default;
		});
		return result;
	}

	getAnimationBlocks() {
		return this.animationActions;
	}

	addAction(actionType, blockIndex) {
		let result = TransitionAnimation.createAction(actionType);
		if (_.isUndefined(blockIndex)) {
			this.getAnimationBlocks().push([result]);
		} else {
			this.getAnimationBlocks()[blockIndex].push(result);
		}
	}

	removeAction(blockIndex, actionIndex) {
		if (_.isUndefined(actionIndex)) {
			this.getAnimationBlocks().splice(blockIndex, 1);
		} else {
			this.getAnimationBlocks()[blockIndex].splice(actionIndex, 1);
		}
	}

	updateAnimationAction(blockIndex, actionIndex, fieldName, value) {
		if (fieldName === 'type') {
			this.getAnimationBlocks()[blockIndex][actionIndex] = TransitionAnimation.createAction(
				value);
		} else {
			this.getAnimationBlocks()[blockIndex][actionIndex][fieldName] = value;
		}

	}
}