import { bindThis } from 'Common';
import Store from './Store';

export default class User extends Store{
	/**
	 * @class
	 * @param id {number}
	 * @param login {string}
	 * @param firstName {string}
	 * @param lastName {string}
	 * @param email {string}
	 * @param avatar {string}
	 * @param statistics {Object}
	 */
	constructor(id, login, firstName, lastName, email, avatar, statistics) {
		super();

		this.id = id;
		this.login = login;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.avatar = avatar;
		this.statistics = statistics;

		bindThis([
			this.getLogin,
			this.getFirstName,
			this.getLastName,
			this.getEmail,
			this.getAvatar,
			this.getStatistics,

			this.setId,
			this.updateStatistics,

			this.logout
		], this);
	}

	getStatistics() {
		return this.statistics;
	}

	updateStatistics(statistics) {
		if (!_.isUndefined(statistics.score)) this.statistics.score = statistics.score;
	}

	getId() {
		return this.id;
	}

	getFirstName() {
		return this.firstName;
	}

	getLastName() {
		return this.lastName;
	}

	getEmail() {
		return this.email;
	}

	getLogin() {
		return this.login;
	}

	getAvatar() {
		return this.avatar;
	}

	setId(id) {
		this.id = id;
	}

	logout() {
		super.Store.dispatch({ type: ACTION_TYPES.LOG_OUT });
	}

	static parse(rawObject) {
		let id = +rawObject.id || 0;
		let login = rawObject.login || '';
		let firstName = rawObject.firstName || '';
		let lastName = rawObject.lastName || '';
		let email = rawObject.email || '';
		let avatar = rawObject.avatar || '';
		let statistics = {
			score: _.get(rawObject.statistics, 'score', 0)
		};

		return new User(id, login, firstName, lastName, email, avatar, statistics);
	}
}